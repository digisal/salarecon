 Files connected to Zakhartsev et al: [SALARECON connects the Atlantic salmon genome to growth and feed efficiency](https://journals.plos.org/ploscompbiol/article?id=10.1371/journal.pcbi.1010194).  
[Memote report](https://digisal.gitlab.io/salarecon/salarecon.html).  
The model is also available in Biomodels as https://identifiers.org/biomodels.db/MODEL2207070001.

| Notebooks reproducing results                                | figures                                                                | description |
|---|---|---|
| [contents.ipynb](code/notebooks/contents.ipynb)                             | [2](figures/fig2.svg), [S1](figures/figs1.svg)                         | Overview of model |
| [feed_aa_limitation.ipynb](code/notebooks/feed_aa_limitation.ipynb)         | [5](figures/fig5.svg), [S5](figures/figs5.svg)                         | Comparison of growth on three different amino acid compositions and with amino acid supplenments |
| [final_curation.ipynb](code/notebooks/final_curation.ipynb)                 |                                                                        | Documentation of the final steps of model curation |
| [oxygen_limited_growth.ipynb](code/notebooks/oxygen_limited_growth.ipynb)   | [4](figures/fig4.svg), [S4](figures/figs4.svg)                         | Oxygen limited growth simulations compared with experimental data |
| [quality_evaluation.ipynb](code/notebooks/quality_evaluation.ipynb)         | [3](figures/fig3.svg), [S2](figures/figs2.svg), [S3](figures/figs3.pdf) | Consistency, performance of metabolic tasks, amino acid essentiality and comparison with different models |
