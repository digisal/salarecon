import os
from memote.suite.api import test_model, snapshot_report, validate_model


def model_results(model_path):
    model, sbml_ver, warnings = validate_model(model_path)
    model, result = test_model(
        model, skip=[], results=True, sbml_version=sbml_ver
    )
    model_name = model_path.split("/")[-1].split(".xml")[0]
    result_path = "results/" + model_name + "/"
    if not os.path.exists(os.path.dirname(result_path)):
        os.makedirs(os.path.dirname(result_path))

    rep = snapshot_report(result)
    with open(model_name + ".html", "w", encoding="utf-8") as f:
        f.write(rep)
        f.close()

model_results('salarecon.xml')
